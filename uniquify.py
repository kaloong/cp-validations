#!/usr/bin/env python
###
### 
### Takes in a raw firewall logfile and merges unique entries to a target file.
###
### The raw logfile fields are:
### "Number" "Date" "Time" "Interface" "Origin" "Type" "Action" "Service" 
### "Source Port" "Source" "Destination" "Protocol" "Rule" "Rule Name" 
### "Current Rule Number" "User" "Information" "Product" "Source Machine Name" "Source User Name"
###
### Unique entries are decided only on the following fields which are merged to the target file:
### "Interface" "Service" "Source" "Destination" "Protocol"
###

import sys
import os
import time
import optparse
import shlex
import shutil
import json
from collections import OrderedDict
from operator import itemgetter


class Uniquify:

	def __init__(self, fwlogfile, targetfile, rulenumber):
		self.entries = []
		self.entries_srt = []
		self.fwlogfile = fwlogfile	
		self.targetfile = targetfile	
		self.rulenumber = rulenumber
		self.curr_time = (time.strftime("%Y%m%d-%H:%M:%S"))

	def loadTargetfile(self):
		with open (self.targetfile) as f:
			try:
				self.entries = json.load(f)
			except ValueError:
				self.entries = []
		return (1, len(self.entries))

	def mergeFirewallLogfile(self):
		merge_count = 0
		with open (self.fwlogfile) as f:
			for line in f:
				try:
					line = shlex.split(line)
					if len(line) == 20 and line[12] == self.rulenumber:
						entry = OrderedDict()
						entry["Source_IP"] = line[9]
						entry["Destination_IP"] = line[10]
						entry["Destination_Port"] = line[7]
						entry["Service"] = line[16].replace('service_id: ','') or 'Unknown'
						entry["Protocol"] = line[11]
						entry['Interface'] = line[3]
						if entry not in self.entries:
							self.entries.append(entry)
							merge_count += 1
				except ValueError:
					return 0
		return (1, merge_count)

	def backupTargetfile(self):
		try:
			shutil.copyfile(self.targetfile, 'out/json/unique_entries.' + self.curr_time)
		except ValueError:
			return 0
		return 1

	def writeTargetfile(self):
		self.entries_srt = sorted(self.entries, key=itemgetter('Destination_IP', 'Source_IP')) 
		with open (self.targetfile, 'w') as f:
			try:
				json.dump(self.entries_srt, f, indent=0, ensure_ascii=False)
			except ValueError:
				return 0

		targetfile_txt = 'out/txt/unique_entries.LATEST'
		with open (targetfile_txt, 'w') as f:
			for line in self.entries_srt:
				try:
					json.dump(line, f, ensure_ascii=False)
					f.write('\n')
				except ValueError:
					return 0
		return (1, len(self.entries))

def parseOptions():
	parser = optparse.OptionParser()
	parser.add_option("-f", "--fwlog-file", dest="fwlogfile",
		help="Firewall log file to be uniquified")
	parser.add_option('-t', "--target-file", default="out/json/unique_entries.LATEST", dest="targetfile",
		help="Target file on which firewall entries are merged")
	parser.add_option('-r', "--rule-number", dest="rulenumber",
		help="Firewall rule number to uniquify - subject to change")
	(options, args) = parser.parse_args()

	if (options.fwlogfile is None) or (options.targetfile is None) or (options.rulenumber is None):
		parser.print_help()
		exit(1)
	elif not os.path.isfile(options.fwlogfile):
		parser.error('Firewall log file does not exist')
	elif not os.path.isfile(options.targetfile):
		parser.error('Target file does not exist.  Create an empty one if merging into an existing one is not required.')

	return options

def main():
	options = parseOptions()
	fwlogfile = options.fwlogfile
	targetfile = options.targetfile
	rulenumber = options.rulenumber

	uniquify = Uniquify(fwlogfile, targetfile, rulenumber)	

	print "\nLoading target file: %s ..." %targetfile
	success, entry_count = uniquify.loadTargetfile()
	if success:
		print "%d entries loaded from targetfile: %s ..." % (entry_count, targetfile)
	else:
		print "Error Loading target file: %s\n Exiting ..." %targetfile
		sys.exit(1)

	print "\nMerging firewall log file: %s ..." %fwlogfile
	success, merge_count = uniquify.mergeFirewallLogfile()
	if success:
		print "%d entries merged from firewall log file: %s ..." % (merge_count, fwlogfile)
	else:
		print "Error merging Firewall log file: %s\n Exiting ..." %fwlogfile
		sys.exit(1)

	print "\nWriting updated target file: %s ..." %targetfile
	if uniquify.backupTargetfile():
		success, entry_count = uniquify.writeTargetfile()
		if success:
			print "%d entries written to target file: %s" % (entry_count, targetfile)
	else:
		print "Error updating target file: %s\n Exiting ..." %targetfile
		sys.exit(1)

	sys.exit(0)

# vi: set noet ts=4 :

if __name__ == '__main__':
	main()
