
# Quick steps for CP logs

1. Use Checkpoint SmartView Tracker to export logs.
   - Ensure IPs are *not* resolved
   - Ensure Services are *not* resolved
   - Ensure that a proper Rule filter is set within Query Properties.  The Rule filter will generally include a range of rules that need to be captured.

2. Download the exported logs

3. Run uniquify.py on each log file.  Mandatory options are the logfile name & a single rule number.  User multiple rule numbers if required.  If starting a fresh batch, ensure the following directory paths are available:
  - out/
  - out/txt
  - out/json
Also touch the out/json/unique_entries.LATEST file.  As unique entries are found in the log files, they will be appended here.

4. The resulting out/json/unique_entries.LATEST file will have unresolved entries.  Resolve them using resolve.py.  Mandatory options are source & target filenames.  The target file could be (say) out/json/unique_entries.LATEST.resolved

5. To import the data into Google spreadsheets, see here for the script: http://blog.fastfedora.com/projects/import-json 
   And add it to your Google spreadsheets.

6.  The unique_entries.LATEST.resolved can now be imported from any public webserver.  There is a S3 bucket in the my AWS account that can be used (called cpweb).  Simply copy your file there & use this function in the spreadsheet:
=importJSON("http://cpweb.s3-website-eu-west-1.amazonaws.com/unique_entries.LATEST.resolved", "/", "noInherit, noTruncate")

