#!/usr/bin/env python
###
### 
### Takes in a raw firewall logfile and merges unique entries to a target file.
###
### The raw logfile fields are:
### "Number" "Date" "Time" "Interface" "Origin" "Type" "Action" "Service" 
### "Source Port" "Source" "Destination" "Protocol" "Rule" "Rule Name" 
### "Current Rule Number" "User" "Information" "Product" "Source Machine Name" "Source User Name"
###
### Unique entries are decided only on the following fields which are merged to the target file:
### "Interface" "Service" "Source" "Destination" "Protocol"
###

import sys
import os
import optparse
import shlex
import json
import socket
import re
import socket
from collections import OrderedDict
from operator import itemgetter


class Resolve:

	def __init__(self, sourcefile, targetfile ):
		self.entries = []
		self.entries_srt = []
		self.ipaddrs = []
		self.sourcefile = sourcefile	
		self.targetfile = targetfile	

	def loadSourcefile(self):
		pattern = re.compile(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
		with open (self.sourcefile) as f:
			try:
				tmpentries = json.load(f)
				for entry in tmpentries:
					if pattern.match(entry['Source_IP']):
						entry['Source'] = self.resolvEntry(entry['Source_IP'])
					if pattern.match(entry['Destination_IP']):
						entry['Destination'] = self.resolvEntry(entry['Destination_IP'])
					self.entries.append(entry)
					sys.stdout.write("Resolved %d of %d   \r" % (len(self.entries), len(tmpentries)))
					sys.stdout.flush()
			except ValueError:
				self.entries = []
		return 1

	def resolvEntry(self, ip):
		for ipaddr in self.ipaddrs:
			if ip == ipaddr['IP']:
				return ipaddr['Host']
		try:
			host_arr = socket.gethostbyaddr(ip)
			host = unicode(host_arr[0], "utf-8")
		except:
			host = ip
		
		self.ipaddrs.append({'IP' : ip, 'Host' : host})	
		return host
				
		
	def writeTargetfile(self):
		self.entries_srt = sorted(self.entries, key=itemgetter('Destination_IP', 'Source_IP')) 
		with open (self.targetfile, 'w') as f:
			try:
				json.dump(self.entries_srt, f, indent=0, ensure_ascii=False)
			except ValueError:
				return 0
		return 1

def parseOptions():
	parser = optparse.OptionParser()
	parser.add_option("-s", "--source-file", dest="sourcefile",
		help="Source file to be uniquified")
	parser.add_option('-t', "--target-file", dest="targetfile",
		help="Target file on which firewall entries are merged")
	(options, args) = parser.parse_args()

	if (options.sourcefile is None) or (options.targetfile is None): 
		parser.print_help()
		exit(1)
	elif not os.path.isfile(options.sourcefile):
		parser.error('Source file does not exist')

	return options

def main():
	options = parseOptions()
	sourcefile = options.sourcefile
	targetfile = options.targetfile

	resolve = Resolve(sourcefile, targetfile)	

	print "Loading source file: %s ..." %sourcefile
	if resolve.loadSourcefile():
		print "Source file loaded successfully: %s ..." %sourcefile
	else:
		print "Error Loading source file: %s\n Exiting ..." %sourcefile
		sys.exit(1)

	print "Writing updated target file: %s ..." %targetfile
	if resolve.writeTargetfile():
		print "Target file updated successfully: %s" %targetfile
	else:
		print "Error updating target file: %s\n Exiting ..." %targetfile
		sys.exit(1)

	sys.exit(0)

# vi: set noet ts=4 :

if __name__ == '__main__':
	main()
